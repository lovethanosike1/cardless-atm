﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardlessAtm
{
    public class Operations
    {
        public static double balance = 0;

        public event Notify TransactionSuccess;

        public Operations()
        {
            double Balance = balance;
        }
        public void MakeDeposit()
        {
            double deposit;
            Console.WriteLine("Make a First Deposit");
            Console.WriteLine("Enter amount:");
            deposit = Double.Parse(Console.ReadLine());

            balance += deposit;
            OnDeposit();
            
            
        }
        protected virtual void OnDeposit()
        {
            TransactionSuccess?.Invoke();
        }
    }

}
