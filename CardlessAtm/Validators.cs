﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardlessAtm
{
    public static class Validators
    {
        public static string GetPin()
        {
        start:
            Console.WriteLine("Select Pin: \nInstruction:Pin should be 4-Digits");

            var pin = Console.ReadLine();
            if (pin.Trim().Length == 4)
            {
                if (!IsNumeric(pin))
                {
                    Console.WriteLine("Pin is not numeric");
                    goto start;
                }
                else
                {
                    Console.WriteLine("Confirm your pin");
                    var confirmPin = Console.ReadLine();

                    var res = PinValidator(pin, confirmPin);
                    if (res == pin)
                    {
                        return pin;
                    }
                    else
                    {
                        Console.WriteLine(res);
                        goto start;

                    }
                }

            }
            else
            {
                Console.WriteLine("Pin should be 4-digits");
                goto start;
            }


        }
        private static string PinValidator(string pin, string confirmPin)
        {
            if (pin.Trim().Length == 4)
            {
                if (pin == confirmPin)
                {
                    return pin;
                }
                else
                {
                    return "pin did not match";
                }

            }
            else
            {
                return "Invalid pin.Pin should be 4-digits";
            }

        }
        public static bool IsNumeric(string pin)
        {
            double test;
            return double.TryParse(pin, out test);

        }
    }
}
