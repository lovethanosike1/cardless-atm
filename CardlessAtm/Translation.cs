﻿using System;


namespace CardlessAtm
{
    public static class Translation
    {
        public static void EnglishWithdrawalPrompt() 
        { Console.WriteLine("Enter Amount:"); }
        public static void IgboWithdrawalPrompt() 
        { Console.WriteLine("Tinye Ego Ichoro: "); }
        public static void PidginWithdrawalPrompt() 
        { Console.WriteLine("Press Amount u want:"); }
        public static void EnglishInsuffientFunds()
        {
            Console.WriteLine("Insufficient funds!");
        }

        public static void IgboInsuffientFunds()
        {
            Console.WriteLine("Inwero Ego!"); 
        }

        public static void PidginInsuffientFunds()
        {
            Console.WriteLine("You No Get Money!");
        }


        public static void EnglishDominationPrompt()
        {
            Console.WriteLine("Input Denomination\n * 500\n * 1000");
        }
        public static void IgboDominationPrompt()
        { Console.WriteLine("Tinye Domination\n * 500\n * 1000"); }
        public static void PidginDominationPrompt()
        { Console.WriteLine("Selectiii Denomination\n * 500\n * 1000"); }
        public static void EnglishBalanceMessage()
        { Console.WriteLine("Your balance is:"); }
        public static void IgboBalanceMessage()
        { Console.WriteLine("Ego foro bu:"); }
        public static void PidginBalanceMessage()
        { Console.WriteLine("Wetin remain na:"); }
        public static void ETryAgainPrompt()
        { Console.WriteLine("Do you want to perform another transcation?\nYes=1   No = 2"); }
        public static void ITryAgainPrompt()
        { Console.WriteLine("I choro ime transaction ozo?\nEeh=1   Mba = 2"); }
        public static void PTryAgainPrompt()
        { Console.WriteLine("You wan do another transcation?\nYes=1   No = 2"); }
        public static void EInvalidPrompt()
        { Console.WriteLine("Invalid input."); }
        public static void IInvalidPrompt()
        { Console.WriteLine("Nke a bughi nke a choro"); }
        public static void PInvalidPrompt()
        { Console.WriteLine("Input no follow"); }
    }

}
