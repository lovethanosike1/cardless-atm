﻿using System;

namespace CardlessAtm
{
    public static class AccountServices
    {
        public static User _user;

        private static string pin;

        private static int VaultAmount = 20000;
        public static void Register(User model)
        {
            _user = model;

            Console.WriteLine($"Your account {_user.FirsName} has been created with an account balance {_user.AccountBalance}");
            Console.WriteLine("Press 1 to Login");
            var input = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(input) || input.Split().Length > 1)
            {
                input = PromptUser("input");
            }
        }

        public static void LoginUser(string pin)
        {
            Console.WriteLine("Enter Pin: ");
            try
            {
                pin = Console.ReadLine();

                if (pin == _user.Pin)
                {
                    Console.WriteLine("---------------------------------------------\n You are logged in");
                    LanguagePrompt();
                }
                else
                {
                    Console.WriteLine("Access denied");
                    pin = PromptPin("Incorrect Pin");
                }

            }
            catch (Exception e)
            { Console.WriteLine(e.Message); }
        }
        public static void LanguagePrompt()
        {
            Console.WriteLine("Select languague\n1.English\n2.Igbo\n3.Pidgin");
            var input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    EnglishOperation();
                    break;
                case "2":
                    IgboOperation();
                    break;
                case "3":
                    PidginOperation();
                    break;
                default:
                    break;
            }
        }

        public static void EnglishOperation()
        {
            Console.WriteLine("Select Option:");
            Console.WriteLine("1 = Withdraw\t2=Check Balance\t3.0=Exit");
            int Input = int.Parse(Console.ReadLine());
            switch (Input)
            {
                case 1:
                    EnglishMakeWithdraw();
                    break;
                case 2:
                    EnglishCheckBalance();
                    break;
                case 0:
                    break;
                default:
                    EnglishOperation();
                    break;
            }
        }
        public static void IgboOperation()
        {
            Console.WriteLine("Ghoro nke I choro:");
            Console.WriteLine("1 = Withdraw(Were ego)\t2=Check Balance(lee ole foro)\t0=Puo");
            int Input = int.Parse(Console.ReadLine());
            switch (Input)
            {
                case 1:
                    IgboMakeWithdraw();
                    break;
                case 2:
                    IgboCheckBalance();
                    break;
                case 0:
                    break;
                default:
                    IgboOperation();
                    break;
            }
        }
        public static void PidginOperation()
        {
            Console.WriteLine("Wetin you wan do:");
            Console.WriteLine("1 = Withdraw\t2=Check Balance\t0=Comot");
            int Input = int.Parse(Console.ReadLine());
            switch (Input)
            {
                case 1:
                    PidginMakeWithdraw();
                    break;
                case 2:
                    PidginCheckBalance();
                    break;
                case 0:
                    break;
                default:
                    PidginOperation();
                    break;
            }
        }

        public static void EnglishMakeWithdraw()
        {
            try
            {
                string amount;
                double Amount;
                Translation.EnglishWithdrawalPrompt();

                amount = Console.ReadLine();
                while (String.IsNullOrWhiteSpace(amount) && !Validators.IsNumeric(amount))
                {

                    amount = PromptUser("amount");
                }
                Amount = double.Parse(amount);

                if (Amount > 0)
                {
                    if (Amount < AccountServices.GetBalance())
                    {
                        if (Amount < VaultAmount)
                        {
                            Translation.EnglishDominationPrompt();
                            double AmountDenomination = Double.Parse(Console.ReadLine());
                            Double DenominationCount = Amount / AmountDenomination;

                            Operations.balance -= Amount;

                            switch (AmountDenomination)
                            {
                                case 500:
                                    AccountServices.DebitBalance(Amount);
                                    Console.WriteLine($"\tPlease wait...\n\t---take your cash, '{Amount}' in {DenominationCount} pieces of {AmountDenomination}");
                                    break;
                                case 1000:
                                    AccountServices.DebitBalance(Amount);
                                    Console.WriteLine($"\tPlease wait...\n\t---take your cash, '{Amount}' in {DenominationCount} pieces of {AmountDenomination}");

                                    break;
                                default:
                                    Console.WriteLine("Amount Denomination is not available");
                                    break;
                            }

                            Translation.ETryAgainPrompt();
                            ETryAgain();

                        }
                        else
                            Translation.EnglishInsuffientFunds();
                    }
                    else
                        Console.WriteLine("Unable to dispense cash");
                }
                else
                    Console.WriteLine("Invalid input");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void IgboMakeWithdraw()
        {
            try
            {
                string amount;
                double Amount;
                Translation.IgboWithdrawalPrompt();
                amount = Console.ReadLine();
                while (String.IsNullOrWhiteSpace(amount) && !Validators.IsNumeric(amount))
                {

                    amount = PromptUser("amount");
                }
                Amount = Double.Parse(amount);

                if (Amount > 0)
                {
                    if (Amount < AccountServices.GetBalance())
                    {
                        if (Amount < VaultAmount)
                        {
                            Translation.IgboDominationPrompt();
                            double AmountDenomination = Double.Parse(Console.ReadLine());
                            double DenominationCount = Amount / AmountDenomination;

                            Operations.balance -= Amount;

                            switch (AmountDenomination)
                            {
                                case 500:
                                    AccountServices.DebitBalance(Amount);
                                    Console.WriteLine($"\tChere...\n\t---were ego gi, '{Amount}' na nkebi '{DenominationCount}'  of {AmountDenomination}");
                                    break;
                                case 1000:
                                    AccountServices.DebitBalance(Amount);
                                    Console.WriteLine($"\tChere...\n\t---were ego gi, '{Amount}' na nkebi '{DenominationCount}' of {AmountDenomination}");

                                    break;
                                default:
                                    Console.WriteLine("Nkebi Ichoro adighi");
                                    break;
                            }
                            Translation.ITryAgainPrompt();
                            ITryAgain();
                        }
                        Console.WriteLine("Ego a adighi na machine");

                    }
                    else
                        Translation.IgboInsuffientFunds();

                }
                else
                    Console.WriteLine("Nke abughi nke a choro");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void PidginMakeWithdraw()
        {
            try
            {
                string amount;
                double Amount;
                Translation.PidginWithdrawalPrompt();
                amount = Console.ReadLine();
                while (String.IsNullOrWhiteSpace(amount) && !Validators.IsNumeric(amount))
                {

                    amount = PromptUser("amount");
                }
                Amount = Double.Parse(amount);

                if (Amount > 0)
                {
                    if (Amount < AccountServices.GetBalance())
                    {
                        if (Amount < VaultAmount)
                        {
                            Translation.PidginDominationPrompt();
                            double AmountDenomination = double.Parse(Console.ReadLine());
                            double DenominationCount = Amount / AmountDenomination;

                            Operations.balance -= Amount;

                            switch (AmountDenomination)
                            {
                                case 500:
                                    AccountServices.DebitBalance(Amount);
                                    Console.WriteLine($"\tAbeg wait...\n\t---take your money, '{Amount}' in {DenominationCount} pieces of {AmountDenomination}");
                                    break;
                                case 1000:
                                    AccountServices.DebitBalance(Amount);
                                    Console.WriteLine($"\tPlease wait...\n\t---take your money, '{Amount}' in {DenominationCount} pieces of {AmountDenomination}");

                                    break;
                                default:
                                    Console.WriteLine("Amount Denomination no dey");
                                    break;
                            }
                            Translation.PTryAgainPrompt();
                            PTryAgain();
                        }
                        else
                            Console.WriteLine("Money no reach");

                    }
                    Console.WriteLine("The machine no fit pay the amount");

                }
                else
                    Console.WriteLine("Your input no follow");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void EnglishCheckBalance()
        {
            Translation.EnglishBalanceMessage();
            Console.WriteLine(AccountServices.GetBalance());
            Translation.ETryAgainPrompt();
        }
        public static void IgboCheckBalance()
        {
            Translation.IgboBalanceMessage();
            Console.WriteLine(AccountServices.GetBalance());
            Translation.ITryAgainPrompt();
        }
        public static void PidginCheckBalance()
        {
            Translation.PidginBalanceMessage();
            Console.WriteLine(AccountServices.GetBalance());
            Translation.PTryAgainPrompt();
        }
        public static void ETryAgain()
        {
            Translation.ETryAgainPrompt();
            int Response = int.Parse(Console.ReadLine());
            switch (Response)
            {
                case 1:
                    LoginUser(pin);
                    break;
                case 2:
                    Console.WriteLine("-------------------------------------------------\nTransaction Ended");
                    Environment.Exit(0);
                    break;
                default:
                    Translation.EInvalidPrompt();
                    break;
            }

        }
        public static void ITryAgain()
        {
            Translation.ITryAgainPrompt();
            int Response = int.Parse(Console.ReadLine());
            switch (Response)
            {
                case 1:
                    LoginUser(pin);
                    break;
                case 2:
                    Console.WriteLine("-------------------------------------------------\nTransaction Agwugo");
                    Environment.Exit(0);
                    break;
                default:
                    Translation.IInvalidPrompt();
                    break;
            }

        }
        public static void PTryAgain()
        {
            string ppin;
            Translation.PTryAgainPrompt();
            int Response = int.Parse(Console.ReadLine());
            switch (Response)
            {
                case 1:
                    LoginUser(pin);
                    break;
                case 2:
                    Console.WriteLine("-------------------------------------------------\nE don End");
                    Environment.Exit(0);
                    break;
                default:
                    Translation.PInvalidPrompt();
                    break;
            }

        }
        public static double GetBalance()
        {
            return Operations.balance;
        }
        public static void DebitBalance(double amount)
        {
            _user.AccountBalance -= amount;
        }
        public static string PromptUser(string fieldname)
        {
            Console.WriteLine($"invalid input {fieldname} ");
            return Console.ReadLine();
        }
        public static string PromptPin(string fieldname)
        {
            Console.WriteLine($"invalid input {fieldname} ");
            return Console.ReadLine();
        }
        public static bool IsNegative(double amount)
        {
            bool test = false;
            test = amount < 0 ? true : false;
            return test;
        }
        public static double PromptAmount(string fieldname)
        {
            Console.WriteLine($"invalid input {fieldname} ");
            return Double.Parse(Console.ReadLine());
        }

    }
}
