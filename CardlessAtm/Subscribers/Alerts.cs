﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardlessAtm.Subscribers
{
    public class Alerts
    {
        public  static void Debit()
        {

            for (int i = 0; i < 10; i++)
            {
                Console.Beep();
            }
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Your Account has been Debited!");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void Credit()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.Beep();
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Your Account has been Credited!");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
