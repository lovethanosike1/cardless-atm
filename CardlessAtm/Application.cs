﻿using CardlessAtm.Interface;
using CardlessAtm.Subscribers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CardlessAtm
{
    public class Application:IApplication
    {
        public static Operations operations = new Operations();
        
        public  void Run()
        {
            var menu = new StringBuilder();
            menu.Append("Hello User!");
            menu.AppendLine("You've been redirected to a Login System");

            Console.WriteLine(menu.ToString());
            Console.WriteLine("Get Registered");
            Console.WriteLine("Enter your Firstname:");
            var firstName = Console.ReadLine().Trim();
            while (string.IsNullOrWhiteSpace(firstName) || firstName.Split().Length > 1)
            {
                firstName = PromptUser("firstname");
            }
            var pin = Validators.GetPin();

            
            //Event subscription
            //ProcessBusinessLogic bl = new ProcessBusinessLogic();
            operations.TransactionSuccess+= Alerts.Credit; // register with an event
            operations.MakeDeposit();



            var user = new User();
            user.FirsName = firstName;
            user.Pin = pin;
            user.AccountBalance = Operations.balance;
            
            AccountServices.Register(user);
            AccountServices.LoginUser(pin);
            
            
           

        }
        public static string PromptUser(string fieldName)
        {
            Console.WriteLine($"Please enter a valid {fieldName}");
            return Console.ReadLine().Trim();
        }

    }
}
    
